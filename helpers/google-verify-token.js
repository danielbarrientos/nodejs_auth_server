

const { OAuth2Client } = require('google-auth-library');


const CLIENT_ID = '388772101477-o9mu5oc635haspl0eiau6aan96tfl301.apps.googleusercontent.com'
const client = new OAuth2Client(CLIENT_ID);



const verifyGoogleToken = async (token) => {

    try {
        const ticket = await client.verifyIdToken({
            idToken: token,
            audience: [
                CLIENT_ID,
                '388772101477-qoce88r91af2npiu5aieru947a46pqn6.apps.googleusercontent.com'
          ], 
        });
      
        const payload = ticket.getPayload();
        console.log('======PAYLOAD ========');
        console.log(payload);

        return {
            name: payload['name'],
            email: payload['email'],
            picture: payload['picture'],
        }
    } catch (error) {
        return null;
    }
}

// verifyGoogleToken().catch(console.error);

module.exports = {
    verifyGoogleToken
}