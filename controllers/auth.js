const {response, json} = require('express')
const { verifyGoogleToken } = require('../helpers/google-verify-token')

const googleAuth = async ( req, res = response) => {

    const token = req.body.token

    if(!token) {
        return res.json({
           
            ok:false,
            msg: 'no token in request'
            
        })
    }
    const googleUser = await verifyGoogleToken(token)
    if(! googleUser){
        return res.status(400).json({
            ok:false,            
        })
    }

    return res.json({
        ok:true,
        googleUser
    });
}

module.exports = {
    googleAuth
}